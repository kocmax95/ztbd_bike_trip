import mongoose from 'mongoose';
const { Schema } = mongoose;

const TripSchema = new Schema({
    tripDuration: Number,
    startTime: String,
    stopTime: String,
    startStation: {
        startStationId: Number,
        startStationName: String,
        startStationLatitude: Number,
        startStationLongtitude: Number,
    },
    endStation: {
        endStationId: Number,
        endStationName: String,
        endStationLatitude: Number,
        endStationLongtitude: Number,
    },
    bike: {
        bikeId: Number
    },
    user: {
        userType: String,
        birthYear: Number,
        gender: Number
    }
});

export const CreateModel = (trip) => {
    return {
        tripDuration: trip[0],
        startTime: trip[1],
        stopTime: trip[2],
        startStation: {
            startStationId: trip[3],
            startStationName: trip[4],
            startStationLatitude: trip[5],
            startStationLongtitude: trip[6],
        },
        endStation: {
            endStationId: trip[7] != 'NULL' ? trip[7] : 0,
            endStationName: trip[8] != 'NULL' ? trip[8] : '',
            endStationLatitude: trip[9] != 'NULL' ? trip[9] : 0,
            endStationLongtitude: trip[10] != 'NULL' ? trip[10] : 0,
        },
        bike: {
            bikeId: trip[11]
        },
        user: {
            userType: trip[12],
            birthYear: 1996,
            gender: trip[14]
        }
    }
}

export const TripModel = mongoose.model('Trip', TripSchema);