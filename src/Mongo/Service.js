import {Save} from './Repository';
import { CreateModel } from './Trip';

export const SaveDocuments = async (csvRows) => {

    for (let index = 1; index < csvRows.length; index++) {
        console.log('Zapisuję dokument : ',index);
        
        const element = csvRows[index];
        const model = CreateModel(element);      
        //console.log('model: ',model);
          
        await Save(model);
    }
}
