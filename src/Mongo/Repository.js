import { TripModel, Trip } from './Trip';

export const Save = async (trip) => {
    return await TripModel.create(trip);
}