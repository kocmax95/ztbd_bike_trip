import {
    SaveSql,
    GetById
} from '../SQL/Station';

const ImportStation = async (csvRow, client) => {
    const startStation = PrepareStartStationObject(csvRow);
    const StartStationExists = await CheckIfExists(startStation, client);

    if (!StartStationExists) {
        console.log('Zapisuję startStation o id : ', startStation.id);
        const sql = SaveSql(startStation);
        await client.query(sql);
    }

    const endStation = PrepareEndStationObject(csvRow);
    if (!endStation.id == null) {
        const EndStationExists = await CheckIfExists(endStation, client);

        if (!EndStationExists) {
            console.log('Zapisuję endStation o id : ', endStation.id);
            const sql = SaveSql(endStation);
            await client.query(sql);
        }
    }

}

const PrepareStartStationObject = (csvRow) => {
    return {
        id: csvRow[3],
        latitude: csvRow[5],
        longtitude: csvRow[6]
    }
}

const PrepareEndStationObject = (csvRow) => {
    return {
        id: csvRow[7],
        latitude: csvRow[9],
        longtitude: csvRow[10]
    }
}

const CheckIfExists = async (station, client) => {
    const sql = GetById(station.id);
    const res = await client.query(sql);
    if (res.rows.length > 0) {
        return true;
    } else {
        return false;
    }
}

export default ImportStation;