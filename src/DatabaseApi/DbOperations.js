const Pool = require("pg").Pool;

export const ConnectWithDataBase = () => {
    const client = new Pool({
        host: "localhost",
        user: "postgres",
        database: "bike_trip_history",
        password: "postgres",
        port: 5432
    });

    return client;
}
