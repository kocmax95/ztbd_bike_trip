import {
    SaveSql,
    GetById
} from '../SQL/Bike';

const ImportBike = async (csvRow, client) => {
    const bike = PrepareBikeObject(csvRow);
    const BikeExists = await CheckIfExists(bike, client);   

    if (!BikeExists) {
        console.log('Zapisuję bike o id : ', bike.id);
        const sql = SaveSql(bike);
        await client.query(sql);
    }
}

const PrepareBikeObject = (csvRow) => {
    return {
        id: csvRow[11]
    }
}

const CheckIfExists = async (bike, client) => {
    const sql = GetById(bike.id);   
    const res = await client.query(sql);    
    if (res.rows.length > 0) {
        return true;
    } else {
        return false;
    }
}

export default ImportBike;