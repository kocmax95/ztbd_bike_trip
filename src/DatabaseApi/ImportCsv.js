import {
    ConnectWithDataBase
} from './DbOperations';
import ImportBike from './ImportBike';
import ImportStation from './ImportStation';
import ImportTrip from './ImportTrip';

const ImportCsv = async (csvRows) => {
    const client = ConnectWithDataBase();    

    await ImportSingleEntity(client, csvRows, ImportBike);
    await ImportSingleEntity(client, csvRows, ImportStation);
    await ImportSingleEntity(client, csvRows, ImportTrip);
}

const ImportSingleEntity = async (client, csvRows, ImportMethod) => {  
    console.log(ImportMethod);
    const count = csvRows.length;

    for (let index = 1; index < count; index++) {
        const row = csvRows[index];   
        await ImportMethod(row, client);
    }
}

export default ImportCsv;