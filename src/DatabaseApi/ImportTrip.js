import {
    SaveSql
} from '../SQL/Trip';

const ImportTrip = async (csvRow, client) => {
    const trip = PrepareTripObject(csvRow);
    console.log('Zapisuję trip o tripduration : ', trip.tripduration);
    const sql = SaveSql(trip);    
    await client.query(sql);

}

const PrepareTripObject = (csvRow) => {
    return {
        tripduration: csvRow[0],
        starttime: csvRow[1],
        stoptime: csvRow[2],
        startstationid: csvRow[3],
        endstationid: csvRow[7],
        bikeid: csvRow[11],
        usertype: csvRow[12],
        birthyear: csvRow[13],
        gender: csvRow[14]
    }
}


export default ImportTrip;