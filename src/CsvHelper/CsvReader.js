const fs = require("fs").promises;
const csv = require('async-csv');

export const ReadCsv = async () => {

    const csvString = await fs.readFile('./src/Data/201306-data.csv', 'utf-8');
    const rows = await csv.parse(csvString);

    return rows;
}