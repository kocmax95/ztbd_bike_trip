import ImportCsv from './DatabaseApi/ImportCsv';
import {SaveDocuments} from './Mongo/Service.js';
import {
    ReadCsv
} from './CsvHelper/CsvReader';
import moment from 'moment';
import mongoose  from 'mongoose';

(async () => {

    try {

        mongoose.connect('mongodb://localhost:27017/triphistory', {
            useNewUrlParser: true,
            useCreateIndex: true
        }, (err) => {
            if (!err) {
                console.log('Connection with database established...');
            } else {
                console.log(err);
            }
        });

        const start = moment();
        console.log('Wczytuję plik...');
        const csvRows = await ReadCsv();
        console.log('Zapisuję do bazy...');
        // await ImportCsv(csvRows);
        await SaveDocuments(csvRows);
        const end = moment();
        const duration = end.diff(start, "seconds");

        console.log('Pomyślnie zaimportowano do bazy!');
        console.log('Czas trwania: ', duration);

    } catch (error) {

        console.log('Wystąpił błąd : ', error);
        return 0;

    }
})();