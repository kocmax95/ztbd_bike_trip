export const SaveSql = (station) => {
    return `INSERT INTO public.station(
        stationid, latitude, longtitude)
        VALUES (${station.id}, ${station.latitude}, ${station.longtitude});`
}

export const GetById = (id) => {
    return `SELECT * FROM public.station WHERE stationid = ${id};`
}