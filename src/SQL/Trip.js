export const SaveSql = (trip) => {
    return `INSERT INTO public.trip(
        tripduration, starttime, stoptime, startstationid, endstationid, bikeid, usertype, birthyear, gender)
        VALUES (${trip.tripduration}, '${trip.starttime}', '${trip.stoptime}', 
            ${trip.startstationid}, ${trip.endstationid}, ${trip.bikeid}, '${trip.usertype}',
             ${trip.birthyear}, ${trip.gender});`
}